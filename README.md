# Pterodactyl-Installer
An installer for Pterodactyl game server management panel (https://pterodactyl.io).  
This was intended for a fresh install of Fedora 29 Server, and may support CentOS in the future.
If you do not use a fresh install, things may be broken, and problems may arise.

# Running the script
You can start the installation with the following command:  
```bash
wget https://gitlab.com/tenten8401/Pterodactyl-Installer/raw/master/install.sh
sudo bash install.sh
```

# You will *NOT* get support in the official Pterodactyl Discord server if this script does not install properly. This is *NOT* an official installation script.