#!/bin/bash
set +x
set -e

DEFAULT_PANEL_INSTALL_PATH="/var/www/pterodactyl"
DEFAULT_DAEMON_INSTALL_PATH="/srv/daemon"
DEFAULT_DAEMON_DATA_PATH="/srv/daemon-data"

INSTALL_PATH=DEFAULT_PANEL_INSTALL_PATH

FEDORA_VERSION=30

PANEL_REPO="https://github.com/pterodactyl/panel"
DAEMON_REPO="https://github.com/pterodactyl/daemon"

USE_SSL=true

# Reset
reset='\033[0m'       # Text Reset

# Regular Colors
black='\033[0;30m'        # Black
red='\033[0;31m'          # Red
green='\033[0;32m'        # Green
yellow='\033[0;33m'       # Yellow
blue='\033[0;34m'         # Blue
purple='\033[0;35m'       # Purple
cyan='\033[0;36m'         # Cyan
white='\033[0;37m'        # White

# Global variables
echo "Grabbing panel release data from GitHub..."
LATEST_PANEL=$(curl -L -s -H 'Accept: application/json' "${PANEL_REPO}/releases/latest")
PANEL_VERSION=$(echo ${LATEST_PANEL} | sed -e 's/.*"tag_name":"\([^"]*\)".*/\1/')

LATEST_DAEMON=$(curl -L -s -H 'Accept: application/json' "${DAEMON_REPO}/releases/latest")
DAEMON_VERSION=$(echo ${LATEST_DAEMON} | sed -e 's/.*"tag_name":"\([^"]*\)".*/\1/')

echo "Generating random passwords..."
PASSWORD=$(head /dev/urandom | tr -dc A-Za-z0-9 | head -c 24)
MYSQL_PASSWORD=$(head /dev/urandom | tr -dc A-Za-z0-9 | head -c 24)
ADMIN_PASSWORD=$(head /dev/urandom | tr -dc A-Za-z0-9 | head -c 24)

# Pre-Flight Checks
if [ "${UID}" -ne "0" ]; then
  echo "You must be logged in as root to run this script."
  exit 87
fi

if [ $(cat /etc/fedora-release) != *"${FEDORA_VERSION}"* ]; then
    echo "You must be using Fedora ${FEDORA_VERSION}. Please upgrade."
    exit 1
fi

install_panel() {
    # Script dependencies
    echo "Installing script dependencies..."
    dnf -y install bind-utils

    # Gather user input
    clear
    echo "Installing Pterodactyl Panel..."
    echo -n "Installation path [${DEFAULT_PANEL_INSTALL_PATH}]: "
    read INSTALL_PATH
    INSTALL_PATH=${INSTALL_PATH:-${DEFAULT_PANEL_INSTALL_PATH}}

    echo -n "Enter Panel URL (not including http(s)://) [$(hostname -f)]: "
    read FQDN
    FQDN=${FQDN:-$(hostname -f)}

    SERVER_IP=$(curl -s http://checkip.amazonaws.com)
    DOMAIN_RECORD=$(dig +short ${FQDN})

    if [ "${SERVER_IP}" != "${DOMAIN_RECORD}" ]
    then
        echo -e "${red}The entered domain does not resolve to the primary public IP of this server.${reset}"
        echo -n "Would you like to continue? [Y/n]? "
        read RESPONSE
        if [[ "${RESPONSE}" =~ ^([nN][oO]|[nN])+$ ]]; then
            exit 1
        fi
    fi

    echo -n "Enter Email [admin@${FQDN}]: "
    read EMAIL
    EMAIL=${EMAIL:-"admin@${FQDN}"}

    echo -n "Enter desired username [admin]: "
    read USERNAME
    USERNAME=${USERNAME:-"admin"}

    echo -n "Do you want to use SSL? [Y/n]: "
    read RESPONSE
    USE_SSL=true
    if [[ "${RESPONSE}" =~ ^([nN][oO]|[nN])+$ ]]; then
        USE_SSL=false
    fi

    echo -ne "
    Email: ${white}${EMAIL}${reset}
    Username: ${white}${USERNAME}${reset}
    URL: ${white}https://${FQDN}/${reset}
    Install path: ${white}${INSTALL_PATH}${reset}
    Use SSL: ${white}${USE_SSL}${reset}
    Are the settings above correct [Y/n]? "

    read RESPONSE
    RESPONSE=${RESPONSE:-y}
    if [[ "$RESPONSE" =~ ^([yY][eE][sS]|[yY])+$ ]]; then
        if [ -d "$INSTALL_PATH" ]; then
            echo -ne "${red}${INSTALL_PATH} already exists, do you want to overwrite [y/N]?${reset} "
            override=${override:-n}
            read override
            if [[ "${override}" =~ ^([nN][oO]|[nN])+$ ]]; then
                exit 1
            fi
        fi
        # Begin actual install
        set -x
        # Install PHP
        dnf -y install php php-cli php-gd php-pdo php-mbstring php-tokenizer php-bcmath php-xml php-fpm php-curl php-zip php-json php-mysqlnd
        systemctl enable --now php-fpm

        # Install Redis
        dnf -y install redis
        systemctl enable --now redis

        # Install MariaDB
        dnf -y install mariadb mariadb-server
        systemctl enable --now mariadb

        # Install nginx
        dnf -y install nginx
        systemctl enable --now nginx

        # Install miscellaneous
        dnf -y install certbot-nginx curl tar unzip git policycoreutils-python-utils

        # Install composer
        curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer

        # Configure MariaDB
        mysql -e "UPDATE mysql.user SET Password = PASSWORD('${MYSQL_PASSWORD}') WHERE User = 'root'"
        mysql -e "DELETE FROM mysql.user WHERE User='';"
        mysql -e "DELETE FROM mysql.user WHERE User='root' AND Host NOT IN ('localhost', '127.0.0.1', '::1');"
        mysql -e "DROP DATABASE IF EXISTS test;"
        mysql -e "DELETE FROM mysql.db WHERE Db='test' OR Db='test\\_%'"
        mysql -e "CREATE DATABASE panel /*\!40100 DEFAULT CHARACTER SET utf8 */;"
        mysql -e "CREATE USER pterodactyl@'127.0.0.1' IDENTIFIED BY '${MYSQL_PASSWORD}';"
        mysql -e "GRANT ALL PRIVILEGES ON panel.* TO 'pterodactyl'@'127.0.0.1';"
        mysql -e "FLUSH PRIVILEGES;"

        # Make dir and install
        mkdir -p "${INSTALL_PATH}"
        curl -Lo "${INSTALL_PATH}/pterodactyl.tar.gz" "${PANEL_REPO}/archive/${PANEL_VERSION}.tar.gz"
        tar --strip-components=1 -xzvf "${INSTALL_PATH}/pterodactyl.tar.gz" -C "${INSTALL_PATH}"
        chmod -R 755 "${INSTALL_PATH}/storage" "${INSTALL_PATH}/bootstrap/cache"
        chown -hR nginx:nginx ${INSTALL_PATH}
        semanage fcontext -a -t httpd_sys_rw_content_t "${INSTALL_PATH}/storage(/.*)?"
        setsebool -P httpd_can_network_connect on
        restorecon -R "${INSTALL_PATH}"
        rm -f "${INSTALL_PATH}/pterodactyl.tar.gz"

        # Copy example env file
        cp "${INSTALL_PATH}/.env.example" "${INSTALL_PATH}/.env"
        cd "${INSTALL_PATH}"

        # Run composer
        composer install --no-dev --optimize-autoloader

        if [ $(which firewall-cmd) ]; then
            # Firewall commands for firewalld
            echo "Configuring firewalld"
            firewall-cmd --add-service=http --permanent
            firewall-cmd --add-service=https --permanent 
            firewall-cmd --reload
        fi

        # Configure PHP-FPM
        echo "Configuring PHP-FPM"
        curl -Lo "/etc/php-fpm.d/pterodactyl.conf" "https://gitlab.com/tenten8401/Pterodactyl-Installer/raw/master/templates/phpfpm-pterodactyl.conf"
        systemctl restart php-fpm

        # Get LE certificate & Configure nginx

        if [ USE_SSL ]; then
            if certbot --nginx -d "${FQDN}" --non-interactive --agree-tos --email "${EMAIL}"; then
                PREFIX="https"
                echo "Certificate acquired successfully, enabling SSL in nginx."
                curl -Lo "/etc/nginx/conf.d/pterodactyl.conf" "https://gitlab.com/tenten8401/Pterodactyl-Installer/raw/master/templates/pterodactyl.conf"
            else
                echo "Getting the certificate failed, disabling SSL in nginx."
                curl -Lo "/etc/nginx/conf.d/pterodactyl.conf" "https://gitlab.com/tenten8401/Pterodactyl-Installer/raw/master/templates/pterodactyl-nossl.conf"
                PREFIX="http"
            fi
        else
            echo "Getting the certificate failed, disabling SSL in nginx."
            curl -Lo "/etc/nginx/conf.d/pterodactyl.conf" "https://gitlab.com/tenten8401/Pterodactyl-Installer/raw/master/templates/pterodactyl-nossl.conf"
            PREFIX="http"
        fi
        sed -i 's@<domain>@'"${FQDN}"'@g' "/etc/nginx/conf.d/pterodactyl.conf"
        sed -i 's@<install directory>@'"${INSTALL_PATH}"'@g' "/etc/nginx/conf.d/pterodactyl.conf"
        systemctl restart nginx

        # Run artisan commands
        php artisan key:generate --force
        php artisan p:environment:setup -n --author="${EMAIL}" --url=${PREFIX}://${FQDN} --timezone=UTC --cache=redis --session=redis --queue=redis --redis-host=127.0.0.1 --redis-pass="" --redis-port=6379
        php artisan p:environment:database -n --host=127.0.0.1 --port=3306 --database=panel --username=pterodactyl --password="${MYSQL_PASSWORD}"
        php artisan migrate --seed --env=local --no-interaction
        php artisan p:user:make -n --email="${EMAIL}" --username="${USERNAME}" --name-first="${USERNAME}" --name-last="${USERNAME}" --password="${ADMIN_PASSWORD}" --admin=1

        # Add scheduling stuff
        echo "Adding systemd timers"
        curl -Lo "/etc/systemd/system/ptero-cron.timer" "https://gitlab.com/tenten8401/Pterodactyl-Installer/raw/master/templates/ptero-cron.timer"
        curl -Lo "/etc/systemd/system/ptero-cron.service" "https://gitlab.com/tenten8401/Pterodactyl-Installer/raw/master/templates/ptero-cron.service"
        sed -i 's@<install directory>@'"${INSTALL_PATH}"'@g' "/etc/systemd/system/ptero-cron.service"
        systemctl daemon-reload
        systemctl enable --now ptero-cron.timer

        echo "Adding pteroq service"
        curl -Lo "/etc/systemd/system/pteroq.service" "https://gitlab.com/tenten8401/Pterodactyl-Installer/raw/master/templates/pteroq.service"
        sed -i 's@<install directory>@'"${INSTALL_PATH}"'@g' "/etc/systemd/system/pteroq.service"
        systemctl daemon-reload
        systemctl enable --now pteroq

        # Print credentials
        set +x
        echo -e "All finished!
Log into your panel installation using the following details:
URL: ${PREFIX}://${FQDN}
Username: ${USERNAME}
Password: ${ADMIN_PASSWORD}

MySQL Details (Save these if needed later):
Username    | Password
root        | ${MYSQL_PASSWORD}"
        echo "Press any key to continue..."
        read
    else
        install_panel
    fi
}

install_daemon() {
    # Script dependencies
    echo "Installing script dependencies..."
    dnf -y install bind-utils
    
    #TODO: despacito
    clear
    echo "Installing Pterodactyl Daemon..."
    echo -n "Installation path [${DEFAULT_DAEMON_INSTALL_PATH}]: "
    read DAEMON_PATH
    DAEMON_PATH=${DAEMON_PATH:-$DEFAULT_DAEMON_INSTALL_PATH}
    echo -n "Daemon data path [${DEFAULT_DAEMON_DATA_PATH}]: "
    read DATA_PATH
    DATA_PATH=${DATA_PATH:-$DEFAULT_DAEMON_DATA_PATH}

    echo -n "Enter Daemon FQDN (not including http(s)://) [$(hostname -f)]: "
    read FQDN
    FQDN=${FQDN:-$(hostname -f)}

    SERVER_IP=$(curl -s http://checkip.amazonaws.com)
    DOMAIN_RECORD=$(dig +short ${FQDN})

    if [ "${SERVER_IP}" != "${DOMAIN_RECORD}" ]
    then
        echo -e "${red}The entered domain does not resolve to the primary public IP of this server.${reset}"
        echo -n "Would you like to continue? [Y/n]? "
        read RESPONSE
        if [[ "${RESPONSE}" =~ ^([nN][oO]|[nN])+$ ]]; then
            exit 1                                                                                                    
        fi                    
    fi

    echo -n "Enter Email [admin@${FQDN}]: "
    read EMAIL
    EMAIL=${EMAIL:-"admin@${FQDN}"}

    echo -n "Do you want to use SSL? [Y/n]: "
    read RESPONSE
    USE_SSL=true
    if [[ "${RESPONSE}" =~ ^([nN][oO]|[nN])+$ ]]; then
        USE_SSL=false
    fi

    echo -ne "

Email: ${white}${EMAIL}${reset}
FQDN: ${white}${FQDN}${reset}
Install path: ${white}${DAEMON_PATH}${reset}
Data path: ${white}${DATA_PATH}${reset}
Use SSL: ${white}${USE_SSL}${reset}
Are the settings above correct [Y/n]? "

    read RESPONSE
    RESPONSE=${RESPONSE:-y}
    if [[ "$RESPONSE" =~ ^([yY][eE][sS]|[yY])+$ ]]; then
        set -x
    
        # Installing Docker
        curl -sSL https://get.docker.com | CHANNEL=stable bash
        systemctl enable --now docker

        # Installing nodejs and deps
        dnf -y install nodejs node-gyp gcc-c++ make curl tar unzip git certbot-nginx policycoreutils-python-utils

        # Create directories
        mkdir -p "${DAEMON_PATH}" "${DATA_PATH}"
        cd "${DAEMON_PATH}"

        # Download the daemon
        curl -L "${DAEMON_REPO}/archive/${DAEMON_VERSION}.tar.gz" | tar --strip-components=1 -xzv
        npm install --only=production

        # Create the cert
        if [ USE_SSL ]; then
            certbot certonly --nginx -d "${FQDN}" --text --non-interactive --agree-tos --email "${EMAIL}" || true
        fi

        if [ $(which firewall-cmd) ]; then
            # Firewall commands for firewalld
            echo "Configuring firewalld"
            # TODO: Don't hardcode port
            firewall-cmd --add-port=8080/tcp --permanent
            firewall-cmd --add-port=2022/tcp --permanent
            firewall-cmd --reload
        fi

        # Add & enable systemd service
        curl -Lo "/etc/systemd/system/wings.service" "https://gitlab.com/tenten8401/Pterodactyl-Installer/raw/master/templates/wings.service"
        systemctl daemon-reload

        echo -e "${yellow}Daemon installation is nearly complete, Please go to the panel and get your \"Auto deploy\" command in the node configuration tab.${reset}"

        echo "Paste your auto deploy command below: "
        read AUTODEPLOY
        ${AUTODEPLOY}
        systemctl enable --now wings
        set +x
    else
        install_daemon
    fi
    
}

clear

echo -e "
Welcome to the Pterodactyl Auto-Installer for Fedora.
This was made for a FRESH install of Fedora ${FEDORA_VERSION}.
${red}You may run into issues if you aren't using a fresh install.${reset}
Please select what you would like to from the list below:

[${cyan}1${reset}] Install Only Panel
[${cyan}2${reset}] Install Only Daemon
[${cyan}3${reset}] Full Install (Panel + Daemon)
[${red}0${reset}] Quit
"

dispatch() {
    echo -n "Enter Selection: "
    read software

    case $software in
        1 )
            clear
            install_panel
            ;;
        2 )
            clear
            install_daemon
            ;;
        3 )
            clear
            install_panel
            install_daemon
            ;;
        0 )
            exit 0
            ;;
        * )
            clear
            echo -e "${red}Invalid selection.${reset}"
            dispatch
            ;;
    esac
}
dispatch